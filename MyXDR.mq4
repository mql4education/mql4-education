#property copyright "Copyright 2018, Chan Yu Pang"
#property link	"http://www.mql5.com/en/users/freezemusic"
#property version "1.00"
#property description ""
#property strict
#include <stderror.mqh>
#include <stdlib.mqh>

extern string RobotName = "MyXDoubler"; //EA Name

extern int magicno = 1501; //Magic Number
extern bool TYPEA = false; //Type A
extern int StopAttempt = 4; //Stop Attempt
extern double DollarAmount = 3.0; //Dollar Amount
extern double EntryLot = 0.01; //Entry Lot

//+------------------------------------------------------------------+
//| INTERNAL VARIABLES                                               |
//+------------------------------------------------------------------+

double lotSize;
string comment = "XDR";
int MaxSlippage = 30; //points
double StopLoss = 20.0; //pips

//MARKET INFO
double M_Low;
double M_High;
double M_Time;
double M_Bid;
double M_Ask;
double M_Point;
double M_Digits;
double M_Spread;
double M_StopLevel;
double M_LotSize;
double M_TickValue;
double M_TickSize;
double M_SwapLong;
double M_SwapShort;
double M_Starting;
double M_Expiration;
double M_TradeAllowed;
double M_MinLot;
double M_MaxLot;
double M_LotStep;
double Spread;

//+------------------------------------------------------------------+
//| 0. UTILITIES                                                     |
//+------------------------------------------------------------------+

void OnInit(){
  HideTestIndicators(true);
}

void OnDeinit(){
}

//+------------------------------------------------------------------+
//| 1. Preliminary                                                   |
//+------------------------------------------------------------------+

bool Preliminary(){
  if(!IsTradeAllowed()){
    Print("*PLEASE ALLOW TRADING");
    return(false);
  }
  if(!IsConnected()){
    Print("*NO CONNECTION");
    return(false);
  }
  Print(RobotName+"INITIALIZED");
  return(true);
}

//+------------------------------------------------------------------+
//| 2. Get Market Info                                               |
//+------------------------------------------------------------------+

int GetMarketInfo(){
  M_Low = MarketInfo(Symbol(), MODE_LOW);
  M_High = MarketInfo(Symbol(), MODE_HIGH);
  M_Time = MarketInfo(Symbol(), MODE_TIME);
  M_Bid = MarketInfo(Symbol(), MODE_BID);
  M_Ask = MarketInfo(Symbol(), MODE_ASK);
  M_Point = MarketInfo(Symbol(), MODE_POINT);
  M_Digits = MarketInfo(Symbol(), MODE_DIGITS);
  M_Spread = MarketInfo(Symbol(), MODE_SPREAD);
  M_StopLevel = MarketInfo(Symbol(), MODE_STOPLEVEL);
  M_LotSize = MarketInfo(Symbol(), MODE_LOTSIZE);
  M_TickValue = MarketInfo(Symbol(), MODE_TICKVALUE);
  M_TickSize = MarketInfo(Symbol(), MODE_TICKSIZE);
  M_SwapLong = MarketInfo(Symbol(), MODE_SWAPLONG);
  M_SwapShort = MarketInfo(Symbol(), MODE_SWAPSHORT);
  M_Starting = MarketInfo(Symbol(), MODE_STARTING);
  M_Expiration = MarketInfo(Symbol(), MODE_EXPIRATION);
  M_TradeAllowed = MarketInfo(Symbol(), MODE_TRADEALLOWED);
  M_MinLot = MarketInfo(Symbol(), MODE_MINLOT);
  M_MaxLot = MarketInfo(Symbol(), MODE_MAXLOT);
  M_LotStep = MarketInfo(Symbol(), MODE_LOTSTEP);
  return (0);
}

//+------------------------------------------------------------------+
//| 3. Order Accounting                                              |
//+------------------------------------------------------------------+

bool isValidLot = true;
int Cal_Lots(){
  string syb = Symbol();
  int totalOrder = 0;
  double result = EntryLot;
  //CALCULATE LOT SIZE
  if(result <= M_MinLot) result = M_MinLot;
  if(result > M_MaxLot) result = M_MaxLot;
  //CHECK LOT STEP
  result = MathRound(result/M_LotStep) * M_LotStep;
  if (result < M_MinLot) result = M_MinLot;
  //FREE MARGIN CHECK
  isValidLot =
    !(AccountFreeMarginCheck(Symbol(), OP_BUY, result) <= 0
    || AccountFreeMarginCheck(Symbol(), OP_SELL, result) <= 0);
  //DISPLAY
  if(!isValidLot){
    Print("*NOT ENOUGH FREE MARGIN");
  }else{
    Print("Lot Size:"+DoubleToStr(result,2));
  }
  //SAVE
  lotSize = result;
  return(0);
}

//+------------------------------------------------------------------+
//| We calculate the actual value of spread (returned functions on   |
//| the market can give the incorrect actual value of spread if the  |
//| broker varies the value of spread                                |
//+------------------------------------------------------------------+

int Cal_Spread(){
  Spread = Ask - Bid;
  Print("Current Spread: "+DoubleToStr(Spread/M_Point,0));
  return (0);
}

//+------------------------------------------------------------------+
//| 5. Logic                                                         |
//+------------------------------------------------------------------+

//ENTER MARKET LOGICS
int FindSymbolOrder(){
  int total = OrdersTotal();
  int ticket = 0;
  for(int i = 0; i < total; i++){
    ticket = OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
    if(OrderSymbol() == Symbol()
      && OrderMagicNumber() == magicno
      && OrderComment() == comment){
      return(ticket);
    }
  }
  return (0);
}
int OrderBuy(){
  int ticket = OrderSend(Symbol(), OP_BUY, lotSize, Ask, MaxSlippage, 0, 0, comment, magicno, 0, clrGreen);
  return(ticket);
}
int OrderSell(){
  int ticket = OrderSend(Symbol(), OP_SELL, lotSize, Bid, MaxSlippage, 0, 0, comment, magicno, 0, clrGreen);
  return(ticket);
}
double MA(int TF, int period, int pos = 0){
  return (iMA(Symbol(),TF,period,1,MODE_SMA,PRICE_CLOSE,pos));
}
double MA_Slope(int TF, int period){
  return (MA(TF,period,2) - MA(TF,period,0));
}
int AskMA(){
  int ticket = 0;
  
  bool BarTouchH1 = High[1] > MA(PERIOD_H1, 12) && MA(PERIOD_H1, 12) > Low[1];
  bool BuyCondition =
    BarTouchH1
    && MA_Slope(PERIOD_H4, 144) > 0
    && MA_Slope(PERIOD_H4, 169) > 0
    && MA_Slope(PERIOD_H4, 34) > 0
    && MA_Slope(PERIOD_H4, 12) > 0;
  bool SellCondition =
    BarTouchH1
    && MA_Slope(PERIOD_H4, 144) < 0
    && MA_Slope(PERIOD_H4, 169) < 0
    && MA_Slope(PERIOD_H4, 34) < 0
    && MA_Slope(PERIOD_H4, 12) < 0;

   
  if(BuyCondition){
    ticket = OrderBuy();
  }else if(SellCondition){
    ticket = OrderSell();
  }
  return (ticket);
}
int EnterMarket(){
  if(!isValidLot)
    return(0);
  int ticket = AskMA();
  if(ticket > 0){
    Print("Order is opened: "+OrderOpenPrice());
  }
  return(0);
}

//EXIT MARKET LOGICS
int StopByDollar(){
  int ticket = FindSymbolOrder();
  if(ticket <= 0) return(0);
  double netGain = OrderProfit()+OrderSwap()+OrderCommission();
  if(netGain >= DollarAmount){ //TP if EARN & TP_ZONE
    if(OrderType() == OP_BUY){
      OrderClose(OrderTicket(), OrderLots(), Bid , MaxSlippage, Violet);
      Print("Buy Order closed (TP): "+Bid);
    }
    if(OrderType() == OP_SELL){
      OrderClose(OrderTicket(), OrderLots(), Ask , MaxSlippage, Violet);
      Print("Sell Order closed (TP): "+Ask);
    }
  }
  if(netGain<0){ //SL if LOSS & SL_ZONE
    if(OrderType() == OP_BUY && OrderOpenPrice() < NormalizeDouble(Ask-StopLoss*10*M_Point, M_Digits)){
      OrderClose(OrderTicket(), OrderLots(), Bid , MaxSlippage, Violet);
      Print("Buy Order closed (SL): "+Bid);
    }
    if(OrderType() == OP_SELL && OrderOpenPrice() > NormalizeDouble(Bid+StopLoss*10*M_Point, M_Digits)){
      OrderClose(OrderTicket(), OrderLots(), Ask , MaxSlippage, Violet);
      Print("Sell Order closed (SL): "+Ask);
    }
  }
  return(0);
}
int ExitMarket(){
  StopByDollar();
  return(0);
}

//CORE LOGIC
int Logic(){
  bool hasOpenedOrder = FindSymbolOrder()>0;
  if(!hasOpenedOrder){
    EnterMarket();
  }else{
    ExitMarket();
  }
  return(0);
}

//+------------------------------------------------------------------+
//| MAIN                                                             |
//+------------------------------------------------------------------+

void OnTick(){
  if(!Preliminary()) //1. Preliminary Check
    return;
  GetMarketInfo();   //2. Get Market Info
  Cal_Lots();        //3. Order Accounting
  Cal_Spread();      //MUST DO
  Logic();           //5. Core Logic
}
